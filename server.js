const express = require('express');
const app = express();

const questionList = [
  (htmlQuestions = [
    {
      question: 'HTML stands for',
      answer: 'Hypertext Markup Language',
      options: [
        'Hypertext Markup Language',
        'High Text Markup Language',
        'Hyper Tabular Markup LanguageTML',
        'None',
      ],
    },
    {
      question:
        'which of the following tag is used to mark a begining of paragraph ?',
      answer: '<P>',
      options: ['&ltTD>', '&ltbr>', '&ltP>', '&ltTR>'],
    },
    {
      question: 'From which tag descriptive list starts ?',
      answer: '<dl>',
      options: ['&ltll>', '&ltdd>', '&ltdl>', '&ltds>'],
    },
    {
      question: 'Correct HTML tag for the largest heading is',
      answer: '<h1>',
      options: ['&lthead>', '&lth6>', '&ltheading>', '&lth1>'],
    },
    {
      question: 'The attribute of <form> tag',
      answer: 'Both (a)&(b)',
      options: ['Method', 'Action', 'Both (a)&(b)', 'None of these'],
    },
  ]),
  (cssQuestions = [
    {
      question:
        'Which of the following selector matches the name of any element type?',
      answer: 'Universal Selector',
      options: [
        'Type selector',
        'Universal Selector',
        'Descendent Selector',
        'class selector',
      ],
    },
  ]),
  (jsQuestions = [
    {
      question:
        'Which of the following is a valid type of function javascript supports?',
      answer: 'Both',
      options: ['named function', 'anonymous function', 'Both', 'none'],
    },
  ]),
];

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

app.get('/getQuestions', (req, res) => {
  res.send(questionList);
});

app.listen(3000, () => {
  console.log('running on port 3000');
});
