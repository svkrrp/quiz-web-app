import { Question } from './question.js';

export class Section {
  constructor() {
    this.execute();
  }

  execute() {
    const sections = document.querySelector('.sections');

    sections.addEventListener('click', (event) => {
      new Question(event.target.closest('section').id);
    });
  }
}
