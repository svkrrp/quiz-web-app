export class Question {
  constructor(id) {
    this.id = id;
    this.score = 0;
    this.initialize();
  }

  calculateScore(questions) {
    return new Promise((resolve, reject) => {
      let score = 0;
      questions.forEach((question, index) => {
        this.create(question).then((result) => {
          console.log(result);
          if (result === question.answer) {
            score++;
          }
          if (index === questions.length - 1) {
            resolve(score);
          }
        });
      });
    });
  }

  submitButtonHandler() {
    alert('you scored ' + this.score);
    console.log(this);
    document.querySelector('main').style.display = 'block';

    const questions = document.querySelector('.questions');
    document.querySelector('body').removeChild(questions);

    const submitButton = document.querySelector('.btn');
    document.querySelector('body').removeChild(submitButton);
  }

  createButton() {
    const submitButton = document.createElement('div');
    submitButton.className = 'btn';
    submitButton.innerHTML = `
      <button>Submit</button>
    `;
    const button = submitButton.querySelector('button');
    button.addEventListener('click', this.submitButtonHandler.bind(this));
    return submitButton;
  }

  createAndHideElement(questionList) {
    // questions lists container
    const questionsEl = document.createElement('div');
    questionsEl.className = 'questions';
    document.querySelector('body').appendChild(questionsEl);

    // question submit button
    const submitButton = this.createButton();
    document.querySelector('body').appendChild(submitButton);

    this.calculateScore(questionList).then((score) => {
      this.score = score;
      console.log(this.score);
    });
  }

  render() {
    if (this.id === 'html') {
      this.createAndHideElement(this.htmlQuestions);
    } else if (this.id === 'css') {
      this.createAndHideElement(this.cssQuestion);
    } else {
      this.createAndHideElement(this.jsQuestion);
    }
  }

  create(question) {
    return new Promise((resolve, reject) => {
      if (document.querySelector('.question-body')) {
        console.log('present');
      } else {
        console.log('not present');
      }
      document.querySelector('.questions').style.display = 'block';
      const questionEl = document.createElement('div');
      questionEl.innerHTML = `<div class="question-body">
        <div class="question-card">
          <div class="question">${question.question}</div>
          <div class="options">
            <div id="option-1" class="option option-1">${question.options[0]}</div>
            <div id="option-2" class="option option-2">${question.options[1]}</div>
            <div id="option-3" class="option option-3">${question.options[2]}</div>
            <div id="option-4" class="option option-4">${question.options[3]}</div>
          </div>
        </div>
      </div>`;
      document.querySelector('main').style.display = 'none';
      document.querySelector('.questions').appendChild(questionEl);
      const options = questionEl.querySelector('.options');
      options.addEventListener('click', (event) => {
        const result = event.target.textContent;
        if (result === question.answer) {
          event.target.style.backgroundColor = 'green';
          this.score++;
        } else {
          event.target.style.backgroundColor = 'red';
        }
        resolve(result);
      });
    });
  }

  async initialize() {
    try {
      const response = await axios.get('http://127.0.0.1:3000/getQuestions');
      this.htmlQuestions = response.data[0];
      this.cssQuestion = response.data[1];
      this.jsQuestion = response.data[2];
      this.render();
    } catch (error) {
      console.log(error);
    }
  }
}
